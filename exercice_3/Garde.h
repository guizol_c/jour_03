#ifndef GARDE_H_
# define GARDE_H_

typedef enum {STARK, LANNISTER, TARGARYEN, BARATHEON, MARTELL, TYRELL} Maison;

typedef struct s_corbeau
{
  char *_nom;
  int _age;
  Maison _maison;
} Corbeau;

void echange(int *l, int *h);
void say(char *snow);
void garde_de_nuit(Corbeau *cor, char *nom, int age, Maison maison);

#endif /* !GARDE_H_ */
